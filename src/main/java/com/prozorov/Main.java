package com.prozorov;

public class Main {

    public static void main(String[] args) {
        char [] fn = System.getenv("VAR_1").toCharArray();
        char [] sn = System.getenv("VAR_2").toCharArray();
        char [] first_num = new char[fn.length];
        char [] second_num = new char[sn.length];
        char minus = '-';

        try {
            if (fn[0] == minus)
                first_num = remove (fn, 0);
            if (sn[0] == minus)
                second_num = remove (sn, 0);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        if (isEqual(first_num,second_num))
            System.out.println("Числа одинаковы по длине");
        if (isGreater(first_num,second_num))
            System.out.println("Первое число длинее второго");
        else
            System.out.println("Первое число короче второго");
    }

    public static boolean isGreater(char [] first_num, char [] second_num){
        if (first_num.length>second_num.length)
            return true;
        return false;
    }

    public static boolean isEqual(char [] first_num, char [] second_num){
        if (first_num.length==second_num.length)
            return true;
        return false;
    }

    private static char [] remove(char [] values, int index) {
        var result = new char [values.length - 1];

        for (var i = 0; i < values.length; i++) {
            if (i != index) {

                var newIndex = i < index ? i : i - 1;
                result[newIndex] = values[i];
            }
        }
        return result;
    }
}