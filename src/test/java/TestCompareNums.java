import org.junit.jupiter.api.Test;

import com.prozorov.Main;

import static org.junit.jupiter.api.Assertions.*;

public class TestCompareNums {
    @Test
    public void testCompareNums(){
        String firstNum = "3333";
        String secondNum = "222";

        assertFalse(Main.isEqual(firstNum.toCharArray(),secondNum.toCharArray()));
        assertTrue(Main.isGreater(firstNum.toCharArray(),secondNum.toCharArray()));
    }
}
